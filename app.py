#!/usr/bin/env python3
from flask import Flask
from redis import Redis, RedisError
import os
import socket


redis = Redis(host='redis', db=0, socket_connect_timeout=2, socket_timeout=2)
app = Flask(__name__)


@app.route('/')
def index():
    try:
        visits = redis.incr('counter')
    except RedisError:
        visits = '0'
    name = os.getenv("NAME", "world")
    hostname = socket.gethostname()
    
    source = f'<h3>Hello {name}!</h3>' \
             f'<b>Hostname:</b> {hostname}<br/>' \
             f'<b>Visits:</b> {visits}'

    return source


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
